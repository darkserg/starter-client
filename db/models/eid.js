'use strict';
module.exports = (sequelize, DataTypes) => {
  const Eid = sequelize.define('Eid', {
    uuid: DataTypes.STRING
  }, {});
  Eid.associate = function(models) {
    // associations can be defined here
  };
  return Eid;
};
