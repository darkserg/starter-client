'use strict';
module.exports = (sequelize, DataTypes) => {
  const BlogPost = sequelize.define('BlogPost', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    html: DataTypes.TEXT,
    isSeeder: DataTypes.BOOLEAN,
  }, {});
  BlogPost.associate = function(models) {
    // associations can be defined here
  };
  return BlogPost;
};
