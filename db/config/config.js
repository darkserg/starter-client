module.exports = {
  development: {
    dialect: "sqlite",
    storage: __dirname + "/../dev.sqlite"
  },
  production: {
    dialect: "sqlite",
    storage: __dirname + "/../prod.sqlite"
  }
  /*"test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": false
  }*/
};
