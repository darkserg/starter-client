import pick from "lodash-es/pick";

const {BlogPost} = require('../../../db/models');

export async function get(req, res) {
	let posts = await BlogPost.findAll();
	let contents = posts.map(p => pick(p, ['title', 'slug']));

	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(JSON.stringify(contents));
}
